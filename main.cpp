#include <iostream>
#include <time.h>
#include <ratio>
#include <chrono>
using namespace std::chrono;
#include "MERA.h"

int main(){
    int rank = 1;
    int chi = 12;
    int dim = 2;
    int layers = 5;
    double epsilon = 1.e-8;
    int q_tensor = 1;
    int q_layer = 1;

    clock_t start = clock();
    high_resolution_clock::time_point t1 = high_resolution_clock::now();


    MERA Ising(rank, chi, dim, layers, epsilon, q_tensor, q_layer);
    Ising.SetIsing(1.0);

    //Ising.Test();    
    Ising.Optimize();

    clock_t end = clock();
    cout << "CPU time used: " << (float)(end-start) / CLOCKS_PER_SEC << " seconds" << endl;
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double> >(t2 - t1);
    cout << "Wall clock time passed: " << time_span.count() << " seconds" << endl;

    return 0;
}
